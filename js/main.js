function randomizeCellInMatrix(a, b) {
    for (var c = function(a, b) {
            return Math.floor(Math.random() * (b - a) + a)
        }, d = c(1, b), e = 0; e < a.length; e++)
        for (var f = 0; f < a[e].length; f++) {
            var g = a[e][f];
            if (g && (d--, !d)) return a[e][f] = 0, {
                x: f,
                y: e
            }
        }
}

function getPixelCordsByMatrixCoords(a, b, c, d) {
    var e = $(c),
        f = e.offset(),
        g = e.height(),
        h = d.offset(),
        i = d.width(),
        j = d.outerWidth(),
        k = (j - i) / 2,
        l = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
        m = b.length,
        n = i / m,
        o = l / m;
    return {
        x: -f.left + a.x * n + h.left + k,
        y: -f.top + a.y * o + o / 2 - g / 2
    }
}! function(a, b) {
    "function" == typeof define && define.amd ? define(b) : a.ScrollMagic = b()
}(this, function() {
    "use strict";
    var a = function() {};
    a.version = "2.0.0", a.Controller = function(c) {
        var e, f, g = "ScrollMagic.Controller",
            h = {
                f: "FORWARD",
                r: "REVERSE",
                p: "PAUSED"
            },
            i = b.defaults,
            j = this,
            k = d.extend({}, i, c),
            l = [],
            m = !1,
            n = 0,
            o = h.p,
            p = !0,
            q = 0,
            r = !0,
            s = function() {
                for (var a in k) i.hasOwnProperty(a) || delete k[a];
                if (k.container = d.get.elements(k.container)[0], !k.container) throw g + " init failed.";
                p = k.container === window || k.container === document.body || !document.body.contains(k.container), p && (k.container = window), q = k.vertical ? d.get.height(k.container) : d.get.width(k.container), k.container.addEventListener("resize", z), k.container.addEventListener("scroll", z), k.refreshInterval = parseInt(k.refreshInterval) || i.refreshInterval, t()
            },
            t = function() {
                k.refreshInterval > 0 && (f = window.setTimeout(A, k.refreshInterval))
            },
            u = function() {
                return k.vertical ? d.get.scrollTop(k.container) : d.get.scrollLeft(k.container)
            },
            v = function(a) {
                k.vertical ? p ? window.scrollTo(d.get.scrollLeft(), a) : k.container.scrollTop = a : p ? window.scrollTo(a, d.get.scrollTop()) : k.container.scrollLeft = a
            },
            w = function() {
                var a = n;
                n = j.scrollPos();
                var b = n - a;
                o = 0 === b ? h.p : b > 0 ? h.f : h.r
            },
            x = function() {
                if (r && m) {
                    var a = d.type.Array(m) ? m : l.slice(0);
                    o === h.r && a.reverse(), a.forEach(function(a) {
                        a.update(!0)
                    }), 0 === a.length && k.loglevel >= 3, m = !1
                }
            },
            y = function() {
                e = d.rAF(x)
            },
            z = function(a) {
                "resize" == a.type && (q = k.vertical ? d.get.height(k.container) : d.get.width(k.container)), w(), m || (m = !0, y())
            },
            A = function() {
                if (!p && q != (k.vertical ? d.get.height(k.container) : d.get.width(k.container))) {
                    var a;
                    try {
                        a = new Event("resize", {
                            bubbles: !1,
                            cancelable: !1
                        })
                    } catch (b) {
                        a = document.createEvent("Event"), a.initEvent("resize", !1, !1)
                    }
                    k.container.dispatchEvent(a)
                }
                l.forEach(function(a) {
                    a.refresh()
                }), t()
            };
        this._options = k;
        var B = function(a) {
            if (a.length <= 1) return a;
            var b = a.slice(0);
            return b.sort(function(a, b) {
                return a.scrollOffset() > b.scrollOffset() ? 1 : -1
            }), b
        };
        return this.addScene = function(b) {
            if (d.type.Array(b)) b.forEach(function(a) {
                j.addScene(a)
            });
            else if (b instanceof a.Scene)
                if (b.controller() !== j) b.addTo(j);
                else if (l.indexOf(b) < 0) {
                l.push(b), l = B(l), b.on("shift.controller_sort", function() {
                    l = B(l)
                });
                for (var c in k.globalSceneOptions) b[c] && b[c].call(b, k.globalSceneOptions[c])
            }
            return j
        }, this.removeScene = function(a) {
            if (d.type.Array(a)) a.forEach(function(a) {
                j.removeScene(a)
            });
            else {
                var b = l.indexOf(a);
                b > -1 && (a.off("shift.controller_sort"), l.splice(b, 1), a.remove())
            }
            return j
        }, this.updateScene = function(b, c) {
            return d.type.Array(b) ? b.forEach(function(a) {
                j.updateScene(a, c)
            }) : c ? b.update(!0) : m !== !0 && b instanceof a.Scene && (m = m || [], -1 == m.indexOf(b) && m.push(b), m = B(m), y()), j
        }, this.update = function(a) {
            return z({
                type: "resize"
            }), a && x(), j
        }, this.scrollTo = function(b) {
            if (d.type.Number(b)) v.call(k.container, b);
            else if (b instanceof a.Scene) b.controller() === j ? j.scrollTo(b.scrollOffset()) : log(2, "scrollTo(): The supplied scene does not belong to this controller. Scroll cancelled.", b);
            else if (d.type.Function(b)) v = b;
            else {
                var c = d.get.elements(b)[0];
                if (c) {
                    var e = k.vertical ? "top" : "left",
                        f = d.get.offset(k.container),
                        g = d.get.offset(c);
                    p || (f[e] -= j.scrollPos()), j.scrollTo(g[e] - f[e])
                } else log(2, "scrollTo(): The supplied argument is invalid. Scroll cancelled.", b)
            }
            return j
        }, this.scrollPos = function(a) {
            return arguments.length ? (d.type.Function(a) && (u = a), j) : u.call(j)
        }, this.info = function(a) {
            var b = {
                size: q,
                vertical: k.vertical,
                scrollPos: n,
                scrollDirection: o,
                container: k.container,
                isDocument: p
            };
            return arguments.length ? void 0 !== b[a] ? b[a] : void 0 : b
        }, this.loglevel = function(a) {
            return arguments.length ? (k.loglevel != a && (k.loglevel = a), j) : k.loglevel
        }, this.enabled = function(a) {
            return arguments.length ? (r != a && (r = !!a, j.updateScene(l, !0)), j) : r
        }, this.destroy = function(a) {
            window.clearTimeout(f);
            for (var b = l.length; b--;) l[b].destroy(a);
            return k.container.removeEventListener("resize", z), k.container.removeEventListener("scroll", z), d.cAF(e), null
        }, s(), j
    };
    var b = {
        defaults: {
            container: window,
            vertical: !0,
            globalSceneOptions: {},
            loglevel: 2,
            refreshInterval: 100
        }
    };
    a.Controller.addOption = function(a, c) {
        b.defaults[a] = c
    }, a.Controller.extend = function(b) {
        var c = this;
        a.Controller = function() {
            return c.apply(this, arguments), this.$super = d.extend({}, this), b.apply(this, arguments) || this
        }, d.extend(a.Controller, c), a.Controller.prototype = c.prototype, a.Controller.prototype.constructor = a.Controller
    }, a.Scene = function(b) {
        var e, f, g = "data-scrollmagic-pin-spacer",
            h = c.defaults,
            i = this,
            j = d.extend({}, h, b),
            k = "BEFORE",
            l = 0,
            m = {
                start: 0,
                end: 0
            },
            n = 0,
            o = !0,
            p = function() {
                for (var a in j) h.hasOwnProperty(a) || delete j[a];
                for (var b in h) x(b);
                v(), i.on("change.internal", function(a) {
                    "loglevel" !== a.what && "tweenChanges" !== a.what && ("triggerElement" === a.what ? s() : "reverse" === a.what && i.update())
                }).on("shift.internal", function() {
                    q(), i.update()
                })
            };
        this.addTo = function(b) {
            return b instanceof a.Controller && f != b && (f && f.removeScene(i), f = b, v(), r(!0), s(!0), q(), f.info("container").addEventListener("resize", t), b.addScene(i), i.trigger("add", {
                controller: f
            }), i.update()), i
        }, this.enabled = function(a) {
            return arguments.length ? (o != a && (o = !!a, i.update(!0)), i) : o
        }, this.remove = function() {
            if (f) {
                f.info("container").removeEventListener("resize", t);
                var a = f;
                f = void 0, a.removeScene(i), i.trigger("remove")
            }
            return i
        }, this.destroy = function(a) {
            return i.trigger("destroy", {
                reset: a
            }), i.remove(), i.off("*.*"), null
        }, this.update = function(a) {
            if (f)
                if (a)
                    if (f.enabled() && o) {
                        var b, c = f.info("scrollPos");
                        b = j.duration > 0 ? (c - m.start) / (m.end - m.start) : c >= m.start ? 1 : 0, i.trigger("update", {
                            startPos: m.start,
                            endPos: m.end,
                            scrollPos: c
                        }), i.progress(b)
                    } else z && "DURING" === k && B(!0);
            else f.updateScene(i, !1);
            return i
        }, this.refresh = function() {
            return r(), s(), i
        }, this.progress = function(a) {
            if (arguments.length) {
                var b = !1,
                    c = k,
                    d = f ? f.info("scrollDirection") : "PAUSED",
                    e = j.reverse || a >= l;
                if (0 === j.duration ? (b = l != a, l = 1 > a && e ? 0 : 1, k = 0 === l ? "BEFORE" : "DURING") : 0 >= a && "BEFORE" !== k && e ? (l = 0, k = "BEFORE", b = !0) : a > 0 && 1 > a && e ? (l = a, k = "DURING", b = !0) : a >= 1 && "AFTER" !== k ? (l = 1, k = "AFTER", b = !0) : "DURING" !== k || e || B(), b) {
                    var g = {
                            progress: l,
                            state: k,
                            scrollDirection: d
                        },
                        h = k != c,
                        m = function(a) {
                            i.trigger(a, g)
                        };
                    h && "DURING" !== c && (m("enter"), m("BEFORE" === c ? "start" : "end")), m("progress"), h && "DURING" !== k && (m("BEFORE" === k ? "start" : "end"), m("leave"))
                }
                return i
            }
            return l
        };
        var q = function() {
                m = {
                    start: n + j.offset
                }, f && j.triggerElement && (m.start -= f.info("size") * j.triggerHook), m.end = m.start + j.duration
            },
            r = function(a) {
                if (e) {
                    var b = "duration";
                    w(b, e.call(i)) && !a && (i.trigger("change", {
                        what: b,
                        newval: j[b]
                    }), i.trigger("shift", {
                        reason: b
                    }))
                }
            },
            s = function(a) {
                var b = 0,
                    c = j.triggerElement;
                if (f && c) {
                    for (var e = f.info(), h = d.get.offset(e.container), k = e.vertical ? "top" : "left"; c.parentNode.hasAttribute(g);) c = c.parentNode;
                    var l = d.get.offset(c);
                    e.isDocument || (h[k] -= f.scrollPos()), b = l[k] - h[k]
                }
                var m = b != n;
                n = b, m && !a && i.trigger("shift", {
                    reason: "triggerElementPosition"
                })
            },
            t = function() {
                j.triggerHook > 0 && i.trigger("shift", {
                    reason: "containerResize"
                })
            },
            u = d.extend(c.validate, {
                duration: function(a) {
                    if (d.type.String(a) && a.match(/^(\.|\d)*\d+%$/)) {
                        var b = parseFloat(a) / 100;
                        a = function() {
                            return f ? f.info("size") * b : 0
                        }
                    }
                    if (d.type.Function(a)) {
                        e = a;
                        try {
                            a = parseFloat(e())
                        } catch (c) {
                            a = -1
                        }
                    }
                    if (a = parseFloat(a), !d.type.Number(a) || 0 > a) throw e ? (e = void 0, 0) : 0;
                    return a
                }
            }),
            v = function(a) {
                a = arguments.length ? [a] : Object.keys(u), a.forEach(function(a) {
                    var b;
                    if (u[a]) try {
                        b = u[a](j[a])
                    } catch (c) {
                        b = h[a]
                    } finally {
                        j[a] = b
                    }
                })
            },
            w = function(a, b) {
                var c = !1,
                    d = j[a];
                return j[a] != b && (j[a] = b, v(a), c = d != j[a]), c
            },
            x = function(a) {
                i[a] || (i[a] = function(b) {
                    return arguments.length ? ("duration" === a && (e = void 0), w(a, b) && (i.trigger("change", {
                        what: a,
                        newval: j[a]
                    }), c.shifts.indexOf(a) > -1 && i.trigger("shift", {
                        reason: a
                    })), i) : j[a]
                })
            };
        this.controller = function() {
            return f
        }, this.state = function() {
            return k
        }, this.scrollOffset = function() {
            return m.start
        }, this.triggerPosition = function() {
            var a = j.offset;
            return f && (a += j.triggerElement ? n : f.info("size") * i.triggerHook()), a
        };
        var y = {};
        this.on = function(a, b) {
            return d.type.Function(b) && (a = a.trim().split(" "), a.forEach(function(a) {
                var c = a.split("."),
                    d = c[0],
                    e = c[1];
                "*" != d && (y[d] || (y[d] = []), y[d].push({
                    namespace: e || "",
                    callback: b
                }))
            })), i
        }, this.off = function(a, b) {
            return a ? (a = a.trim().split(" "), a.forEach(function(a) {
                var c = a.split("."),
                    d = c[0],
                    e = c[1] || "",
                    f = "*" === d ? Object.keys(y) : [d];
                f.forEach(function(a) {
                    for (var c = y[a] || [], d = c.length; d--;) {
                        var f = c[d];
                        !f || e !== f.namespace && "*" !== e || b && b != f.callback || c.splice(d, 1)
                    }
                    c.length || delete y[a]
                })
            }), i) : i
        }, this.trigger = function(b, c) {
            if (b) {
                var d = b.trim().split("."),
                    e = d[0],
                    f = d[1],
                    g = y[e];
                g && g.forEach(function(b) {
                    f && f !== b.namespace || b.callback.call(i, new a.Event(e, b.namespace, i, c))
                })
            }
            return i
        };
        var z, A;
        i.on("shift.internal", function(a) {
            var b = "duration" === a.reason;
            ("AFTER" === k && b || "DURING" === k && 0 === j.duration) && B(), b && C()
        }).on("progress.internal", function() {
            B()
        }).on("add.internal", function() {
            C()
        }).on("destroy.internal", function(a) {
            i.removePin(a.reset)
        });
        var B = function(a) {
                if (z && f) {
                    var b = f.info();
                    if (a || "DURING" !== k) {
                        var c = {
                                position: A.inFlow ? "relative" : "absolute",
                                top: 0,
                                left: 0
                            },
                            e = d.css(z, "position") != c.position;
                        A.pushFollowers ? j.duration > 0 && ("AFTER" === k && 0 === parseFloat(d.css(A.spacer, "padding-top")) ? e = !0 : "BEFORE" === k && 0 === parseFloat(d.css(A.spacer, "padding-bottom")) && (e = !0)) : c[b.vertical ? "top" : "left"] = j.duration * l, d.css(z, c), e && C()
                    } else {
                        "fixed" != d.css(z, "position") && (d.css(z, {
                            position: "fixed"
                        }), C());
                        var g = d.get.offset(A.spacer, !0),
                            h = j.reverse || 0 === j.duration ? b.scrollPos - m.start : Math.round(l * j.duration * 10) / 10;
                        g[b.vertical ? "top" : "left"] += h, d.css(z, {
                            top: g.top,
                            left: g.left
                        })
                    }
                }
            },
            C = function() {
                if (z && f && A.inFlow) {
                    var a = "DURING" === k,
                        b = f.info("vertical"),
                        c = A.spacer.children[0],
                        e = d.isMarginCollapseType(d.css(A.spacer, "display")),
                        g = {};
                    A.relSize.width || A.relSize.autoFullWidth ? a ? d.get.width(window) == d.get.width(A.spacer.parentNode) ? d.css(z, {
                        width: A.relSize.autoFullWidth ? "100%" : "inherit"
                    }) : d.css(z, {
                        width: d.get.width(A.spacer)
                    }) : d.css(z, {
                        width: "100%"
                    }) : (g["min-width"] = d.get.width(b ? z : c, !0, !0), g.width = a ? g["min-width"] : "auto"), A.relSize.height ? a ? d.get.height(window) == d.get.height(A.spacer.parentNode) ? d.css(z, {
                        height: "inherit"
                    }) : d.css(z, {
                        height: d.get.height(A.spacer)
                    }) : d.css(z, {
                        height: "100%"
                    }) : (g["min-height"] = d.get.height(b ? c : z, !0, !e), g.height = a ? g["min-height"] : "auto"), A.pushFollowers && (g["padding" + (b ? "Top" : "Left")] = j.duration * l, g["padding" + (b ? "Bottom" : "Right")] = j.duration * (1 - l)), d.css(A.spacer, g)
                }
            },
            D = function() {
                f && z && "DURING" === k && !f.info("isDocument") && B()
            },
            E = function() {
                f && z && "DURING" === k && ((A.relSize.width || A.relSize.autoFullWidth) && d.get.width(window) != d.get.width(A.spacer.parentNode) || A.relSize.height && d.get.height(window) != d.get.height(A.spacer.parentNode)) && C()
            },
            F = function(a) {
                f && z && "DURING" === k && !f.info("isDocument") && (a.preventDefault(), f.scrollTo(f.info("scrollPos") - (a[f.info("vertical") ? "wheelDeltaY" : "wheelDeltaX"] / 3 || 30 * -a.detail)))
            };
        this.setPin = function(a, b) {
            var c = {
                pushFollowers: !0,
                spacerClass: "scrollmagic-pin-spacer"
            };
            if (b = d.extend({}, c, b), a = d.get.elements(a)[0], !a) return i;
            if ("fixed" === d.css(a, "position")) return i;
            if (z) {
                if (z === a) return i;
                i.removePin()
            }
            z = a;
            var e = z.parentNode.style.display,
                f = ["top", "left", "bottom", "right", "margin", "marginLeft", "marginRight", "marginTop", "marginBottom"];
            z.parentNode.style.display = "none";
            var h = "absolute" != d.css(z, "position"),
                j = d.css(z, f.concat(["display"])),
                k = d.css(z, ["width", "height"]);
            z.parentNode.style.display = e, !h && b.pushFollowers && (b.pushFollowers = !1);
            var l = z.parentNode.insertBefore(document.createElement("div"), z),
                m = d.extend(j, {
                    position: h ? "relative" : "absolute",
                    boxSizing: "content-box",
                    mozBoxSizing: "content-box",
                    webkitBoxSizing: "content-box"
                });
            if (h || d.extend(m, d.css(z, ["width", "height"])), d.css(l, m), l.setAttribute(g, ""), d.addClass(l, b.spacerClass), A = {
                    spacer: l,
                    relSize: {
                        width: "%" === k.width.slice(-1),
                        height: "%" === k.height.slice(-1),
                        autoFullWidth: "auto" === k.width && h && d.isMarginCollapseType(j.display)
                    },
                    pushFollowers: b.pushFollowers,
                    inFlow: h
                }, !z.___origStyle) {
                z.___origStyle = {};
                var n = z.style,
                    o = f.concat(["width", "height", "position", "boxSizing", "mozBoxSizing", "webkitBoxSizing"]);
                o.forEach(function(a) {
                    z.___origStyle[a] = n[a] || ""
                })
            }
            return A.relSize.width && d.css(l, {
                width: k.width
            }), A.relSize.height && d.css(l, {
                height: k.height
            }), l.appendChild(z), d.css(z, {
                position: h ? "relative" : "absolute",
                margin: "auto",
                top: "auto",
                left: "auto",
                bottom: "auto",
                right: "auto"
            }), (A.relSize.width || A.relSize.autoFullWidth) && d.css(z, {
                boxSizing: "border-box",
                mozBoxSizing: "border-box",
                webkitBoxSizing: "border-box"
            }), window.addEventListener("scroll", D), window.addEventListener("resize", D), window.addEventListener("resize", E), z.addEventListener("mousewheel", F), z.addEventListener("DOMMouseScroll", F), B(), i
        }, this.removePin = function(a) {
            if (z) {
                if ("DURING" === k && B(!0), a || !f) {
                    var b = A.spacer.children[0];
                    if (b.hasAttribute(g)) {
                        var c = A.spacer.style,
                            e = ["margin", "marginLeft", "marginRight", "marginTop", "marginBottom"];
                        margins = {}, e.forEach(function(a) {
                            margins[a] = c[a] || ""
                        }), d.css(b, margins)
                    }
                    A.spacer.parentNode.insertBefore(b, A.spacer), A.spacer.parentNode.removeChild(A.spacer), z.parentNode.hasAttribute(g) || (d.css(z, z.___origStyle), delete z.___origStyle)
                }
                window.removeEventListener("scroll", D), window.removeEventListener("resize", D), window.removeEventListener("resize", E), z.removeEventListener("mousewheel", F), z.removeEventListener("DOMMouseScroll", F), z = void 0
            }
            return i
        };
        var G, H = [];
        return i.on("destroy.internal", function(a) {
            i.removeClassToggle(a.reset)
        }), this.setClassToggle = function(a, b) {
            var c = d.get.elements(a);
            return 0 !== c.length && d.type.String(b) ? (H.length > 0 && i.removeClassToggle(), G = b, H = c, i.on("enter.internal_class leave.internal_class", function(a) {
                var b = "enter" === a.type ? d.addClass : d.removeClass;
                H.forEach(function(a) {
                    b(a, G)
                })
            }), i) : i
        }, this.removeClassToggle = function(a) {
            return a && H.forEach(function(a) {
                d.removeClass(a, G)
            }), i.off("start.internal_class end.internal_class"), G = void 0, H = [], i
        }, p(), i
    };
    var c = {
        defaults: {
            duration: 0,
            offset: 0,
            triggerElement: void 0,
            triggerHook: .5,
            reverse: !0,
            loglevel: 2
        },
        validate: {
            offset: function(a) {
                if (a = parseFloat(a), !d.type.Number(a)) throw 0;
                return a
            },
            triggerElement: function(a) {
                if (a = a || void 0) {
                    var b = d.get.elements(a)[0];
                    if (!b) throw 0;
                    a = b
                }
                return a
            },
            triggerHook: function(a) {
                var b = {
                    onCenter: .5,
                    onEnter: 1,
                    onLeave: 0
                };
                if (d.type.Number(a)) a = Math.max(0, Math.min(parseFloat(a), 1));
                else {
                    if (!(a in b)) throw 0;
                    a = b[a]
                }
                return a
            },
            reverse: function(a) {
                return !!a
            }
        },
        shifts: ["duration", "offset", "triggerHook"]
    };
    a.Scene.addOption = function(a, b, d, e) {
        a in c.defaults || (c.defaults[a] = b, c.validate[a] = d, e && c.shifts.push(a))
    }, a.Scene.extend = function(b) {
        var c = this;
        a.Scene = function() {
            return c.apply(this, arguments), this.$super = d.extend({}, this), b.apply(this, arguments) || this
        }, d.extend(a.Scene, c), a.Scene.prototype = c.prototype, a.Scene.prototype.constructor = a.Scene
    }, a.Event = function(a, b, c, d) {
        d = d || {};
        for (var e in d) this[e] = d[e];
        return this.type = a, this.target = this.currentTarget = c, this.namespace = b || "", this.timeStamp = this.timestamp = Date.now(), this
    };
    var d = a._util = function(a) {
        var b, c = {},
            d = function(a) {
                return parseFloat(a) || 0
            },
            e = function(b) {
                return b.currentStyle ? b.currentStyle : a.getComputedStyle(b)
            },
            f = function(b, c, f, g) {
                if (c = c === document ? a : c, c === a) g = !1;
                else if (!l.DomElement(c)) return 0;
                b = b.charAt(0).toUpperCase() + b.substr(1).toLowerCase();
                var h = (f ? c["offset" + b] || c["outer" + b] : c["client" + b] || c["inner" + b]) || 0;
                if (f && g) {
                    var i = e(c);
                    h += "Height" === b ? d(i.marginTop) + d(i.marginBottom) : d(i.marginLeft) + d(i.marginRight)
                }
                return h
            },
            g = function(a) {
                return a.replace(/^[^a-z]+([a-z])/g, "$1").replace(/-([a-z])/g, function(a) {
                    return a[1].toUpperCase()
                })
            };
        c.extend = function(a) {
            for (a = a || {}, b = 1; b < arguments.length; b++)
                if (arguments[b])
                    for (var c in arguments[b]) arguments[b].hasOwnProperty(c) && (a[c] = arguments[b][c]);
            return a
        }, c.isMarginCollapseType = function(a) {
            return ["block", "flex", "list-item", "table", "-webkit-box"].indexOf(a) > -1
        };
        var h = 0,
            i = ["ms", "moz", "webkit", "o"],
            j = a.requestAnimationFrame,
            k = a.cancelAnimationFrame;
        for (b = 0; !j && b < i.length; ++b) j = a[i[b] + "RequestAnimationFrame"], k = a[i[b] + "CancelAnimationFrame"] || a[i[b] + "CancelRequestAnimationFrame"];
        j || (j = function(b) {
            var c = (new Date).getTime(),
                d = Math.max(0, 16 - (c - h)),
                e = a.setTimeout(function() {
                    b(c + d)
                }, d);
            return h = c + d, e
        }), k || (k = function(b) {
            a.clearTimeout(b)
        }), c.rAF = j.bind(a), c.cAF = k.bind(a);
        var l = c.type = function(a) {
            return Object.prototype.toString.call(a).replace(/^\[object (.+)\]$/, "$1").toLowerCase()
        };
        l.String = function(a) {
            return "string" === l(a)
        }, l.Function = function(a) {
            return "function" === l(a)
        }, l.Array = function(a) {
            return Array.isArray(a)
        }, l.Number = function(a) {
            return !l.Array(a) && a - parseFloat(a) + 1 >= 0
        }, l.DomElement = function(a) {
            return "object" == typeof HTMLElement ? a instanceof HTMLElement : a && "object" == typeof a && null !== a && 1 === a.nodeType && "string" == typeof a.nodeName
        };
        var m = c.get = {};
        return m.elements = function(b) {
            var c = [];
            if (l.String(b)) try {
                b = document.querySelectorAll(b)
            } catch (d) {
                return c
            }
            if ("nodelist" === l(b) || l.Array(b))
                for (var e = 0, f = c.length = b.length; f > e; e++) {
                    var g = b[e];
                    c[e] = l.DomElement(g) ? g : m.elements(g)
                } else(l.DomElement(b) || b === document || b === a) && (c = [b]);
            return c
        }, m.scrollTop = function(b) {
            return b && "number" == typeof b.scrollTop ? b.scrollTop : a.pageYOffset || 0
        }, m.scrollLeft = function(b) {
            return b && "number" == typeof b.scrollLeft ? b.scrollLeft : a.pageXOffset || 0
        }, m.width = function(a, b, c) {
            return f("width", a, b, c)
        }, m.height = function(a, b, c) {
            return f("height", a, b, c)
        }, m.offset = function(a, b) {
            var c = {
                top: 0,
                left: 0
            };
            if (a && a.getBoundingClientRect) {
                var d = a.getBoundingClientRect();
                c.top = d.top, c.left = d.left, b || (c.top += m.scrollTop(), c.left += m.scrollLeft())
            }
            return c
        }, c.addClass = function(a, b) {
            b && (a.classList ? a.classList.add(b) : a.className += " " + b)
        }, c.removeClass = function(a, b) {
            b && (a.classList ? a.classList.remove(b) : a.className = a.className.replace(RegExp("(^|\\b)" + b.split(" ").join("|") + "(\\b|$)", "gi"), " "))
        }, c.css = function(a, b) {
            if (l.String(b)) return e(a)[g(b)];
            if (l.Array(b)) {
                var c = {},
                    d = e(a);
                return b.forEach(function(a) {
                    c[a] = d[g(a)]
                }), c
            }
            for (var f in b) {
                var h = b[f];
                h == parseFloat(h) && (h += "px"), a.style[g(f)] = h
            }
        }, c
    }(window || {});
    return a
}),
function(a, b) {
    "function" == typeof define && define.amd ? define(["ScrollMagic", "TweenMax", "TimelineMax"], b) : b(a.ScrollMagic || a.jQuery && a.jQuery.ScrollMagic, a.TweenMax || a.TweenLite, a.TimelineMax || a.TimelineLite)
}(this, function(a, b, c) {
    "use strict";
    var d = "animation.gsap",
        e = Function.prototype.bind.call(console && (console.error || console.log) || function() {}, console);
    a || e("(" + d + ") -> ERROR: The ScrollMagic main module could not be found. Please make sure it's loaded before this plugin or use an asynchronous loader like requirejs."), b || e("(" + d + ") -> ERROR: TweenLite or TweenMax could not be found. Please make sure GSAP is loaded before ScrollMagic or use an asynchronous loader like requirejs."), a.Scene.addOption("tweenChanges", !1, function(a) {
        return !!a
    }), a.Scene.extend(function() {
        var a, e = this,
            f = function() {
                e._log && (Array.prototype.splice.call(arguments, 1, 0, "(" + d + ")", "->"), e._log.apply(this, arguments))
            };
        e.on("progress.plugin_gsap", function() {
            g()
        }), e.on("destroy.plugin_gsap", function(a) {
            e.removeTween(a.reset)
        });
        var g = function() {
            if (a) {
                var b = e.progress(),
                    c = e.state();
                a.repeat && -1 === a.repeat() ? "DURING" === c && a.paused() ? a.play() : "DURING" === c || a.paused() || a.pause() : b != a.progress() && (0 === e.duration() ? b > 0 ? a.play() : a.reverse() : e.tweenChanges() && a.tweenTo ? a.tweenTo(b * a.duration()) : a.progress(b).pause())
            }
        };
        e.setTween = function(d, h, i) {
            var j;
            arguments.length > 1 && (arguments.length < 3 && (i = h, h = 1), d = b.to(d, h, i));
            try {
                j = c ? new c({
                    smoothChildTiming: !0
                }).add(d) : d, j.pause()
            } catch (k) {
                return f(1, "ERROR calling method 'setTween()': Supplied argument is not a valid TweenObject"), e
            }
            if (a && e.removeTween(), a = j, d.repeat && -1 === d.repeat() && (a.repeat(-1), a.yoyo(d.yoyo())), e.tweenChanges() && !a.tweenTo && f(2, "WARNING: tweenChanges will only work if the TimelineMax object is available for ScrollMagic."), a && e.controller() && e.triggerElement() && e.loglevel() >= 2) {
                var l = b.getTweensOf(e.triggerElement()),
                    m = e.controller().info("vertical");
                l.forEach(function(a, b) {
                    var c = a.vars.css || a.vars,
                        d = m ? void 0 !== c.top || void 0 !== c.bottom : void 0 !== c.left || void 0 !== c.right;
                    return d ? (f(2, "WARNING: Tweening the position of the trigger element affects the scene timing and should be avoided!"), !1) : void 0
                })
            }
            if (parseFloat(TweenLite.version) >= 1.14)
                for (var n, o, p = a.getChildren ? a.getChildren(!0, !0, !1) : [a], q = function() {
                        f(2, "WARNING: tween was overwritten by another. To learn how to avoid this issue see here: https://github.com/janpaepke/ScrollMagic/wiki/WARNING:-tween-was-overwritten-by-another")
                    }, r = 0; r < p.length; r++) n = p[r], o !== q && (o = n.vars.onOverwrite, n.vars.onOverwrite = function() {
                    o && o.apply(this, arguments), q.apply(this, arguments)
                });
            return f(3, "added tween"), g(), e
        }, e.removeTween = function(b) {
            return a && (b && a.progress(0).pause(), a.kill(), a = void 0, f(3, "removed tween (reset: " + (b ? "true" : "false") + ")")), e
        }
    })
});
var isMobile = function() {
        return "undefined" != typeof window.orientation
    }(),
    pageLoaded = !1,
    minimalLoadTimeCounter = 0,
    minimalLoadTimeInterval = setInterval(function() {
        minimalLoadTimeCounter++, minimalLoadTimeCounter >= 3 && pageLoaded && (clearInterval(minimalLoadTimeInterval), $(".loader").fadeOut(function() {
            $(".preloader").fadeOut(function() {
                $("body").addClass("loaded"), $(".preloader").remove()
            })
        }))
    }, 1e3);
$(document).ready(function() {
    $("body").addClass("ready").toggleClass("mobile", isMobile)
}), $(window).load(function() {
    $("iframe[data-src]").each(function(a, b) {
        var c = $(b);
        c.attr("src", c.attr("data-src"))
    }), pageLoaded = !0
}), $(document).ready(function() {
    function a(a) {
        a.preventDefault(), e.expand.hide(), e.moreInfo.addClass(c)
    }

    function b(a) {
        a.preventDefault(), e.moreInfo.removeClass(c), e.expand.show()
    }
    var c = "expand",
        d = $("#about"),
        e = {
            content: d.find(".content"),
            expand: d.find('a[data-action="open"]'),
            collapse: d.find('a[data-action="close"]'),
            moreInfo: d.find(".should-collapse")
        };
    e.expand.on("touchstart click", a), e.collapse.on("touchstart click", b)
});

$(document).ready(function() {
    if (!isMobile) {
        var a = $(".intro .letter"),
            b = [],
            c = new ScrollMagic.Controller,
            d = $("h1.logo"),
            e = new ScrollMagic.Scene({
                duration: 900,
                offset: 0
            }).setPin(".intro", {
                pushFollowers: !1
            });
        b.push(e);
        for (var f = 0; f < a.length; f++) {
            var g = randomizeCellInMatrix(logoMatrix, a.length - f),
                h = getPixelCordsByMatrixCoords(g, logoMatrix, a[f], d),
                i = (new TimelineMax).add(TweenMax.from(a[f], 1.2, {
                    css: {
                        bezier: {
                            curviness: 1.25,
                            autoRotate: !1,
                            values: [h]
                        }
                    },
                    ease: Power1.easeInOut
                })),
                j = new ScrollMagic.Scene({
                    duration: 600,
                    offset: 0
                }).setTween(i);
            b.push(j)
        }
        var k = (new TimelineMax).add(TweenMax.to(".subtitle, .datetime", 1.2, {
                css: {
                    opacity: 1
                },
                ease: Power1.easeInOut
            })),
            l = new ScrollMagic.Scene({
                duration: 100,
                offset: 500
            }).setTween(k);
        b.push(l);
        var m = (new TimelineMax).add(TweenMax.to(".will-be-censored", 1.2, {
                className: "+=hide-letter"
            })).add(TweenMax.to(".will-be-censored", 1.2, {
                className: "+=is-censored"
            })),
            n = new ScrollMagic.Scene({
                duration: 100,
                offset: 600
            }).setTween(m);
        b.push(n);
        var o = (new TimelineMax).add(TweenMax.to(".intro", 1.2, {
                scale: .8,
                ease: Power1.easeInOut
            })),
            p = new ScrollMagic.Scene({
                duration: 200,
                offset: 700
            }).setTween(o);
        b.push(p);
        var q = (new TimelineMax).add(TweenMax.to(".corner-ribbon", 1.2, {
                opacity: .9,
                color: "#00d09d",
                ease: Power1.easeInOut
            })),
            r = new ScrollMagic.Scene({
                duration: 200,
                offset: 700
            }).setTween(q);
        b.push(r), c.addScene(b)
    }
}), $(document).ready(function() {
    $.ajax(document.location.pathname).then(function(a) {
        var b = $(".source-viewer > code");
        b.attr("content", a)
    });
    var a = [],
        b = new ScrollMagic.Controller,
        c = (new TimelineMax).add(TweenMax.to(".source-viewer", 1.2, {
            css: {
                y: "-=3000"
            },
            ease: Power1.easeInOut
        })),
        d = new ScrollMagic.Scene({
            duration: document.body.scrollHeight,
            offset: 900
        }).setTween(c);
    a.push(d), b.addScene(a)
}), $(document).ready(function() {
    var a = new ScrollMagic.Controller,
        b = $(".nav a");
    $(b).each(function(b, c) {
        var d = $(c),
            e = d.attr("href"),
            f = $(e);
        new ScrollMagic.Scene({
            triggerElement: d.attr("href"),
            duration: f.outerHeight(),
            offset: 400,
            triggerHook: "onEnter"
        }).setClassToggle(c, "active").addTo(a)
    }), a.scrollTo(function(a) {
        TweenMax.to(window, .5, {
            scrollTo: {
                y: a - 0
            }
        })
    }), $(document).on("click", "a[href^=#]", function(b) {
        var c = $(this).attr("href");
        $(c).length > 0 && (b.preventDefault(), a.scrollTo(c), window.history && window.history.pushState && history.pushState("", document.title, c))
    })
}), $(document).ready(function() {
    var a = {
            body: $("body"),
            handler: $("#navToggle"),
            links: $(".nav-link")
        },
        b = function(b) {
            a.body.toggleClass("nav-active", b)
        };
    a.handler.click(b), a.links.click(function() {
        b(!1)
    })
}), $(document).ready(function() {
    var a = $(".speakers").height();
    isMobile && (a += 50), $("#speakers").css("padding-bottom", a);
    var b = $(".schedule-timetable").height();
    if (b += isMobile ? 100 : 200, $("#schedule").css("padding-bottom", b), isMobile) return void $(".agenda-article").on("click", function() {
        $(this).toggleClass("expanded")
    });
    var c = [],
        d = new ScrollMagic.Controller,
        e = $(".section"),
        f = $(window).height();
    e.each(function(a, b) {
        var d, e = $(b),
            g = e.outerHeight(),
            h = e.outerWidth(),
            i = e.offset().left,
            j = $('<div class="section-illustration"></div>'),
            k = "-=300",
            l = Math.max(f, 1.5 * g);
        e.after(j), d = j.width(), j.css({
            left: h - d + i + 100 + "px",
            top: "-30px"
        });
        var m = $(b).next(".section-illustration").get(0),
            n = (new TimelineMax).add(TweenMax.to(m, 1.2, {
                css: {
                    top: k
                },
                ease: Power1.easeInOut
            })),
            o = new ScrollMagic.Scene({
                triggerElement: b,
                duration: l
            }).setTween(n);
        c.push(o)
    });
    var g = (new TimelineMax).add(TweenMax.to(".speakers", 1.2, {
            css: {
                y: "-=150"
            },
            ease: Power1.easeInOut
        })),
        h = $("section#speakers"),
        i = new ScrollMagic.Scene({
            triggerElement: h.get(0),
            duration: h.outerHeight()
        }).setTween(g);
    c.push(i);
    var j = (new TimelineMax).add(TweenMax.to(".venue-map", 1.2, {
            css: {
                y: "-=140"
            },
            ease: Power1.easeInOut
        })),
        k = $("section#venue"),
        l = new ScrollMagic.Scene({
            triggerElement: k.get(0),
            duration: k.outerHeight()
        }).setTween(j);
    c.push(l);
    var m = (new TimelineMax).add(TweenMax.to(".footer", 1.2, {
            css: {
                y: "-=1000"
            },
            ease: Power1.easeInOut
        })),
        n = $("section:last-of-type"),
        o = new ScrollMagic.Scene({
            triggerElement: n.get(0),
            duration: 1500
        }).setTween(m);
    c.push(o), d.addScene(c)
}), $(document).ready(function() {
    function a() {
        var a = {
                lat: 32.0767793,
                "long": 34.7852898
            },
            b = {
                zoom: 14,
                scrollwheel: !1,
                center: new google.maps.LatLng(a.lat, a["long"]),
                styles: [{
                    featureType: "landscape",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 65
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "poi",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 51
                    }, {
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "road.highway",
                    stylers: [{
                        saturation: -100
                    }, {
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "road.arterial",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 30
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "road.local",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 40
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "transit",
                    stylers: [{
                        saturation: -100
                    }, {
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "administrative.province",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "water",
                    elementType: "labels",
                    stylers: [{
                        visibility: "on"
                    }, {
                        lightness: -25
                    }, {
                        saturation: -100
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{
                        hue: "#ffff00"
                    }, {
                        lightness: -25
                    }, {
                        saturation: -97
                    }]
                }]
            },
            c = document.querySelector(".venue-map");
        
    }

});